<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html>

<head>
    <title> DB Connection Test Project </title>

    <style type="text/css">
        body{margin: 0;background-color: #eee}
        h1{font-family: cursive; background-color: #2e87d4; color: #fff; text-align: center; padding: 20px; }
        div{margin: 200px auto; background-color:#fff; text-align:center; border:1px solid #c3c3c3 }
        a{padding: 20px; margin: 20px; border-radius: 3px; background-color:#66c35a; text-decoration:none; display:inline-block; color: #fff; }
        a:hover{background-color: #ce4cba; transition: all 0.3s}
    </style>

</head>

<body>


<h1> Database Operation Using Servlet </h1>

<div>
    <a href="Login.jsp"> Login </a>
    <a href="Register.jsp"> Register </a>
    <a href="Update.jsp"> Update </a>
    <a href="Delete.jsp"> Delete </a>
</div>



</body>

</html>