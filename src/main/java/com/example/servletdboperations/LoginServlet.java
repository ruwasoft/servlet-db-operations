package com.example.servletdboperations;

import jakarta.servlet.*;
import jakarta.servlet.http.*;
import jakarta.servlet.annotation.*;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.*;

@WebServlet(name = "LoginServlet", value = "/LoginServlet")
public class LoginServlet extends HttpServlet {

    @Override
    public void service(ServletRequest servletRequest, ServletResponse servletResponse) throws IOException {

        servletResponse.setContentType("text/html");
        PrintWriter out = servletResponse.getWriter();

        out.println("<html>");
        out.println("<head>");
        out.println("<title>Login</title>");
        out.println("<style>");
        out.println("body{font-family: cursive;background-color: #f5f5f5}");
        out.println("div{width: 400px;margin: 0px auto;box-shadow: 0px 1px 10px #a8a8a8;background-color: #fff;padding-bottom: 10px;position: absolute;top: 50%;left: 50%;transform: translate(-50%,-50%)}");
        out.println("h1{background-color: #d1a134;text-align: center;margin-top: 0;padding: 20px;color: #fff}");
        out.println("input{display: block; margin:auto; font-size: 20px}");
        out.println("p{text-align: center;font-size: 20px;color: #6a595b;margin;0}");
        out.println("input{width: 70%;border: 1px solid #34cbd1;background-color: #fff;outline: none;border-radius: 5px;height:40px;padding: 10px}");
        out.println("input[type='submit']:hover{background-color: #54b0bc;border: 0}");
        out.println("input[type='submit']{background-color: #34cbd1;color: #fff;cursor: pointer};");
        out.println("</style>");
        out.println("</head>");
        out.println("<body>");
        out.println("<div>");

        Connection con = null;
        Statement stm = null;
        ResultSet rs = null;

        String email = servletRequest.getParameter("email");
        String pass = servletRequest.getParameter("pass");

        try
        {
            Class.forName("com.mysql.jdbc.Driver");
            con = DriverManager.getConnection("jdbc:mysql://localhost:3306/demo","root","");
            String query = "select * from Tblstudent where email='"+email+"' and password = '"+pass+"'";

            stm = con.createStatement();

            rs = stm.executeQuery(query);

            if(rs.next())
            {
                out.println("<h1> Welcome </h1> ");
                out.println("<p>Name - "+rs.getString("name")+"</p>");
                out.println("<p>Roll No - "+rs.getString("roll")+"</p>");
                out.println("<p>Email - "+rs.getString("email")+"</p>");
                out.println("<p>Course - "+rs.getString("course")+"</p>");
            }
            else
            {
                out.println("<h1 style='background-color: #eb1616'>Invalid email or password !</h1>");
            }
        }

        catch(SQLException | ClassNotFoundException e)
        {
            out.println(e);
        }
        finally
        {
            try
            {
                rs.close();
                stm.close();
                con.close();
            }
            catch(SQLException e)
            {
                out.println(e);
            }
        }


        out.println("<p><a href='Login.jsp'>Back</a></p>");
        out.println("</div>");
        out.println("</body>");
        out.println("</html>");

    }

}
