
-- This file containing SQL for create table in database. The database name is 'demo'.
-- First create the database named 'demo' and after that create 'TblStudent' table inside that db from flowing commands.


-- Database: `demo`
-- --------------------------------------------------------

--
-- Table structure for table `tblstudent`
--

CREATE TABLE `tblstudent` (
  `Name` varchar(50) NOT NULL,
  `Roll` int(3) NOT NULL,
  `Email` varchar(50) NOT NULL,
  `Password` varchar(50) NOT NULL,
  `Course` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `tblstudent`
--

INSERT INTO `tblstudent` (`Name`, `Roll`, `Email`, `Password`, `Course`) VALUES
('Ruwan Sampath', 68, 'ruwans@gmail.com', '1997', 'BSC SE'),
('Ruwan', 123, 'ruwasoft@gmail.com', '1234', 'HNDIT'),
('testUpdate', 111, 'ruwa@g.com', '1234', '1234');
COMMIT;


